# statuspage

A simple configurable status page for your hosted services. Reports issues via
email.

## Building

The usual rust build system. Just use `cargo build --release` if unsure and use
the resulting binary in `target/release/statuspage`. Once this is "stable
software" I will submit a PKGBUILD to the AUR.

## Configuration

The configuration file uses YAML and is supplied by path in the first argument
of the server. Until I make better documentation, you can infer usage from this
example:

```yaml
title: "metamuffin.org service status"
bind: 127.0.0.1:8000
interval: 60 # check interval in seconds

mail: # the entire mail section is optional
  mail_from: status@example.org
  mail_to: [admin@example.org]
  smtp_auth: !plain # can be !plain, !starttls or !tls
  smtp_server: 127.0.0.1
  smtp_port: 8025
#   smtp_username: admin # optional smtp auth
#   smtp_password: hunter2

services:
    - title: keks-meet
      url: https://meet.metamuffin.org/ # url shown on the page.
            # each service has a number of "checks" that need to
            # be successful for the service to be considered working.
      checks:
          - !pacman keks-meet-server # this uses pacman -Q to check the installed version of some software
          - !systemd keks-meet-server # this checks the ActiveState of a systemd service
          - !http { url: https://meet.metamuffin.org/ } # this checks that status code of a HTTP request for success

    - title: failing service
      mail_to: [bob@example.org] # for this service, bob will be notified too
      checks:
          - !shell { title: "Do nothing unsuccessfully", command: "false" } # this runs a command and checks the exit code
```

## License

AGPL-3.0-only; See `COPYING`.
