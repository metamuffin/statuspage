use crate::{Config, Status, STATUS};
use serde_json::{json, Value};
use std::{ops::Deref, sync::Arc};

pub async fn make_json_response(config: Arc<Config>) -> Value {
    let status = STATUS.read().await;
    let status = status.deref();
    let total_err = status
        .values()
        .map(|s| s.status.is_err() as usize)
        .sum::<usize>();
    let total_ok = status
        .values()
        .map(|s| s.status.is_ok() as usize)
        .sum::<usize>();

    let mut titles_failed = Vec::new();
    let services = config
        .services
        .iter()
        .enumerate()
        .map(|(i, s)| {
            let total_err = status
                .range((i, usize::MIN)..(i, usize::MAX))
                .map(|(_, s)| s.status.is_err() as usize)
                .sum::<usize>();
            if total_err != 0 {
                titles_failed.push(&s.title)
            }
            json!({
                "status": if total_err == 0 {
                    "ok"
                } else  {
                    "failed"
                },
                "title": &s.title,
            })
        })
        .collect::<Vec<_>>();

    json!({
        "status": if total_err == 0 {
            "ok"
        } else if total_ok == 0 {
            "failed"
        } else {
            "degraded"
        },
        "total_ok": total_ok,
        "total_err": total_err,
        "services": services,
        "failed": titles_failed
    })
}

impl Status {
    pub fn json(&self) -> Value {
        json!({
            "ok": self.status.is_ok(),
            "message": match &self.status { Ok(m) => m, Err(m) => m },
            "time": self.time.timestamp()
        })
    }
}
