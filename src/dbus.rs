use anyhow::{anyhow, Result};
use serde::{Deserialize, Serialize};
use std::{
    collections::{btree_map::Entry::*, BTreeMap},
    path::PathBuf,
    sync::Arc,
};
use tokio::sync::Mutex;
use zbus::{
    address::{
        transport::{Transport, Unix, UnixSocket},
        Address,
    },
    connection::Builder,
    proxy,
    zvariant::{OwnedObjectPath, Type},
    Connection, Proxy,
};

static CONNECTIONS: Mutex<(BTreeMap<String, Arc<Connection>>, Option<Arc<Connection>>)> =
    Mutex::const_new((BTreeMap::new(), None));

#[derive(Debug, Type, Deserialize, Serialize)]
struct ServiceStatus {
    name: String,
    description: String,
    loaded: String,
    active: String,
    substate: String,
    following_state: String,
    unit_path: OwnedObjectPath,
    job_id: u32,
    job_type: String,
    job_path: OwnedObjectPath,
}

#[proxy(
    interface = "org.freedesktop.systemd1.Manager",
    default_service = "org.freedesktop.systemd1",
    default_path = "/org/freedesktop/systemd1"
)]
trait Manager {
    async fn list_units(&self) -> Result<Vec<ServiceStatus>>;
    async fn get_unit(&self, unit_name: &str) -> Result<OwnedObjectPath>;
}

async fn ensure_system_conn() -> Result<Arc<Connection>> {
    let mut connections = CONNECTIONS.lock().await;

    match &connections.1 {
        Some(system_bus) => Ok(system_bus.clone()),
        None => {
            let system_bus = Arc::new(Connection::system().await?);
            connections.1 = Some(system_bus.clone());
            Ok(system_bus)
        }
    }
}

async fn ensure_user_conn(user: &str) -> Result<Arc<Connection>> {
    let mut connections = CONNECTIONS.lock().await;

    match connections.0.entry(user.to_owned()) {
        Occupied(oe) => Ok(oe.get().clone()),
        Vacant(ve) => {
            let mut path = PathBuf::new();
            path.push("/run/user");
            path.push(format!(
                "{}",
                users::get_user_by_name(user)
                    .ok_or(anyhow!("Couldn't find user"))?
                    .uid()
            ));
            path.push("bus");

            let trans = Transport::Unix(Unix::new(UnixSocket::File(path)));

            let conn = Builder::address(Address::new(trans))?.build().await?;
            Ok(ve.insert(Arc::new(conn)).clone())
        }
    }
}

pub(crate) async fn check_systemd_all(user: Option<&str>) -> Result<String> {
    let conn = match user {
        None => ensure_system_conn().await,
        Some(username) => ensure_user_conn(username).await,
    }?;

    let manager = ManagerProxy::new(&conn).await?;

    let (good, bad) = manager.list_units().await?.into_iter().fold(
        (0, vec![]),
        |(old_good, mut old_bad), unit| {
            if matches!(
                unit.substate.as_str(),
                "active"
                    | "inactive"
                    | "plugged"
                    | "mounted"
                    | "mounting"
                    | "dead"
                    | "listening"
                    | "running"
                    | "exited"
                    | "waiting"
                    | "abandoned"
                    | "elapsed"
                    | "start"
            ) {
                (old_good + 1, old_bad)
            } else {
                old_bad.push(format!("{}: {}", unit.name, unit.substate));
                (old_good, old_bad)
            }
        },
    );

    if bad.is_empty() {
        Ok(format!("{good} good services"))
    } else {
        Err(anyhow!("Bad services: {}", bad.join(", ")))
    }
}

pub(crate) async fn check_systemd_unit(user: Option<&str>, unit: &str) -> Result<String> {
    let conn = match user {
        None => ensure_system_conn().await,
        Some(username) => ensure_user_conn(username).await,
    }?;

    let manager = ManagerProxy::new(&conn).await?;
    let unit_object_path = if unit.contains('.') {
        manager.get_unit(unit).await
    } else {
        manager.get_unit(&format!("{unit}.service")).await
    }?;

    let unit_proxy = Proxy::new(
        &conn,
        "org.freedesktop.systemd1",
        unit_object_path,
        "org.freedesktop.systemd1.Unit",
    )
    .await?;

    let substate: String = unit_proxy.get_property("SubState").await?;
    if matches!(
        substate.as_str(),
        "active"
            | "inactive"
            | "plugged"
            | "mounted"
            | "dead"
            | "listening"
            | "running"
            | "exited"
            | "waiting"
            | "abandoned"
            | "elapsed"
    ) {
        Ok(format!("{unit}: {substate}"))
    } else {
        Err(anyhow!("{unit}: {substate}"))
    }
}
