use crate::{mail::send_mail, Config, Status, STATUS};
use std::{
    collections::{BTreeMap, VecDeque},
    sync::Arc,
};
use tokio::sync::RwLock;

static LAST_STATUS: RwLock<BTreeMap<usize, bool>> = RwLock::const_new(BTreeMap::new());
pub static LOG: RwLock<VecDeque<Event>> = RwLock::const_new(VecDeque::new());

pub struct Event {
    pub service: usize,
    pub check: usize,
    pub status: Status,
}

pub async fn update_service(
    config: Arc<Config>,
    service: usize,
    check: usize,
    status: Status,
) -> anyhow::Result<()> {
    let mut last_status = LAST_STATUS.write().await;
    let last_status = last_status.entry(service).or_insert(true);
    let current_status = {
        let status = STATUS.read().await;
        !status
            .range((service, usize::MIN)..(service, usize::MAX))
            .any(|(_, v)| v.status.is_err())
    };

    if *last_status != current_status {
        *last_status = current_status;
        let mut log = LOG.write().await;
        log.push_front(Event {
            status: status.clone(),
            service,
            check,
        });
        while log.len() > 32 {
            log.pop_back();
        }
        if let Err(error) = status.status {
            send_mail(&config, service, check, error).await?;
        }
    }
    Ok(())
}
